package {
    import flash.errors.IOError;
    import flash.errors.IllegalOperationError;
    import flash.events.SecurityErrorEvent;
    import flash.net.URLVariables;

    public class BasePlayer {
        
        protected var state:int = PlayState.IDEL;
        protected var listener:PlayerStateListener;

        public function BasePlayer() {
        }
        
        public function setListener(listener:PlayerStateListener):void {
            this.listener = listener;
        }

        public function getState():int {
            return state;
        }
        
        public function setState(state:int) {
            this.state = state;
            if (listener != null) {
                listener.onPlayStateChanged(state);
            }
        }
        
        public function load(url:String, duration:int=-1):Boolean {
            throw new IllegalOperationError("Not implemented"); 
        }

        public function play():Boolean {
            throw new IllegalOperationError("Not implemented"); 
        }
        
        public function pause():Boolean {
            throw new IllegalOperationError("Not implemented"); 
        }

        public function stop():Boolean {
            throw new IllegalOperationError("Not implemented"); 
        }
        
        public function seek(time:int):Boolean {
            throw new IllegalOperationError("Not implemented"); 
        }
        
        public function getPlayback():int {
            throw new IllegalOperationError("Not implemented"); 
        }
        
        public function getDuration():int {
            throw new IllegalOperationError("Not implemented"); 
        }
        
        public function getType():String {
            throw new IllegalOperationError("Not implemented"); 
        }
    }
}