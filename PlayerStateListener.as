package {
    import flash.display.Loader;
    import flash.events.Event;

    public interface PlayerStateListener {
        function onPlayStateChanged(state:int):void;
        function onFailure(e:Event):void;
        function onTick():void;
        function onPlayerReady():void;
        function log(message:String):void;
    }
}