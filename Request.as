package  {
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.SecurityErrorEvent;
    import flash.net.URLLoader;
    import flash.net.URLLoaderDataFormat;
    import flash.net.URLRequest;
    import flash.net.URLRequestHeader;
    import flash.net.URLRequestMethod;
    import flash.net.URLVariables;

    public class Request {
        private var onCompleteListener:Function;
        private var onIOErrorListener:Function;
        private var onSecurityErrorListener:Function;
        private var request:URLRequest;
        
        public function Request(url:String, method:String, params:URLVariables,
                    onCompleteListener:Function, onSecurityErrorListener:Function, onIOErrorListener:Function) {
            request = new URLRequest();
            trace("request to " + url);
            request.url = url;
            if (params != null) {
                request.data = params;
            }
            request.requestHeaders = [new URLRequestHeader("Content-Type", "application/json")];
            request.method = URLRequestMethod.GET;
            
            this.onCompleteListener = onCompleteListener;    
            this.onSecurityErrorListener = onSecurityErrorListener;
            this.onIOErrorListener = onIOErrorListener;
        }

        public function send() {
            var loader = new URLLoader();
            loader.dataFormat = URLLoaderDataFormat.TEXT;
            loader.addEventListener(Event.COMPLETE, receive);
            loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityErrorListener);
            loader.addEventListener(IOErrorEvent.IO_ERROR, onIOErrorListener);
            loader.load(request);
        }
        
        public function receive(event:Event):void {
            trace("got response:" + event.target.data);
            var result:Object = JSON.parse(event.target.data);
            onCompleteListener(result);
        }
    }
}