package {
    public interface ShareControlListener {
        function onShareLayerToggle(isVisible:Boolean):void;
    }
}