package {
    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.Graphics;
    import flash.display.Loader;
    import flash.display.SimpleButton;
    import flash.display.Sprite;
    import flash.display.Stage;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.MouseEvent;
    import flash.events.SecurityErrorEvent;
    import flash.events.TextEvent;
    import flash.geom.Matrix;
    import flash.net.URLRequest;
    import flash.net.URLRequestMethod;
    import flash.net.navigateToURL;
    import flash.system.LoaderContext;
    import flash.system.Security;
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormat;
    import flash.text.TextFormatAlign;
    
    import fl.controls.Button;
    import fl.controls.ProgressBar;
    import fl.controls.ProgressBarMode;

    public class ControlLayer extends Sprite {
//        private const ARTISTNAME_COLOR:uint = 0xf03eb1;
        private const ARTISTNAME_COLOR:uint = 0x88898c;
        private const TRACKNAME_COLOR:uint = 0x2e2f33;
        private const BG_COLOR:uint = 0xf5f5f5;
        private const BORDER_COLOR:uint = 0xdddddd;
        private const PLAYBACK_COLOR:uint = 0xcd85ff;
        private const DURATION_COLOR:uint = 0xdcdde3;
        private const PLAYBACK_BG_COLOR:uint = 0x000000;

        private const WAVEFORM_TOTAL_COLOR:uint = 0xbbbbbb;
        private const WAVEFORM_PLAYBACK_COLOR:uint = 0x909090;
        private const WAVEFORM_DROP_COLOR:uint = 0xcd85ff;
        private const WAVEFORM_DROP_WITH_PLAYBACK_COLOR = 0xa046de; 

        private const WAVEFORM_HOVER_TOTAL_COLOR:uint = 0xaeaeae;
        private const WAVEFORM_HOVER_PLAYBACK_COLOR:uint = 0x808080;
        private const WAVEFORM_HOVER_DROP_COLOR:uint = 0xc073f5;
        private const WAVEFORM_HOVER_DROP_WITH_PLAYBACK_COLOR = 0x923cce; 

        private const BIN_WIDTH:int = 3;
        private const BIN_HEIGHT:int = 40;
        private const BIN_MARGIN:int = 1;
        
        private var listener:PlayControlListener;

        private var artworkLoader:Loader;
        private var videoLoader:Loader;
        private var aspectRatio:Number = -1;
        private var artworkButton:SimpleButton;
        private var playButton:SimpleButton;
        private var pauseButton:SimpleButton;

        [Embed(source='assets/default_cover.png')]
        private var DefaultCoverAsset:Class;

        [Embed(source='assets/btn_pause__down.png')]
        private var PauseDownAsset:Class;

        [Embed(source='assets/btn_pause__normal.png')]
        private var PauseNormalAsset:Class;

        [Embed(source='assets/btn_pause__hover.png')]
        private var PauseHoverAsset:Class;

        [Embed(source='assets/btn_play__down.png')]
        private var PlayDownAsset:Class;

        [Embed(source='assets/btn_play__normal.png')]
        private var PlayNormalAsset:Class;

        [Embed(source='assets/btn_play__hover.png')]
        private var PlayHoverAsset:Class;

        [Embed(source='assets/btn_share__down.png')]
        private var ShareDownAsset:Class;

        [Embed(source='assets/btn_share__normal.png')]
        private var ShareNormalAsset:Class;

        [Embed(source='assets/btn_share__hover.png')]
        private var ShareHoverAsset:Class;

        [Embed(source='assets/btn_share__selected.png')]
        private var ShareSelectedAsset:Class;

        [Embed(source='assets/player_logo__normal.png')]
        private var LogoNormalAsset:Class;

        [Embed(source='assets/player_logo__hover.png')]
        private var LogoHoverAsset:Class;

        [Embed(source='assets/handle.png')]
        private var ProgressHandleAsset:Class;
        
        private var defaultCoverImg:Bitmap;
        private var progressHandleImg:Bitmap;
        private var linkUrl:String;
        private var artistNameLabel:TextField;
        private var trackNameLabel:TextField;
        
        private var playbackLabel:TextField;
        private var durationLabel:TextField;
        
        private var artistNameFormat:TextFormat;
        private var trackNameFormat:TextFormat;
        private var playbackFormat:TextFormat;
        private var durationFormat:TextFormat;
        private var progressBarTimeFormat:TextFormat;
        
        private var waveformSprite:Sprite;
        
        private var waveformData:Array;
        private var downsizedWaveformData:Array;

        private var soundPlayer:BasePlayer;
        private var currentPlayback:int = 0;
        private var currentDuration:int = 0;
        
        private var dropStart:int = -1;
        private var dropDuration:int = 20;
        
        private var mouseWaveformDragging:Boolean = false;
        private var mouseWaveformHover:Boolean = false;
        
        private var isWaveformLoaded:Boolean = false;
        private var isArtworkLoaded:Boolean = false;
        private var useProgressBar:Boolean = false;
        
        private var artworkWidth:Number = 136;
        private var logoButton:SimpleButton;
        private var shareButton:SimpleButton;
        private var isShareBtnSelected:Boolean = false;
        
        private var shareNormalImg;
        private var shareHoverImg;
        private var shareDownImg;
        private var shareSelectedImg;

        private var trackName:String;
        private var trackNameLink:String;
        private var artistName:String;
        private var artistNameLink:String;

        public function ControlLayer(listener:PlayControlListener):void {
            this.listener = listener;
        }
        
        public function reset():void {
            soundPlayer = null;

            waveformData = new Array();
            downsizedWaveformData = new Array();

            if (waveformSprite != null) {
                waveformSprite.graphics.clear();
            }
            playbackLabel.text = "00:00";
            playbackLabel.setTextFormat(playbackFormat);
            
            durationLabel.text = "00:00";
            durationLabel.setTextFormat(durationFormat);

            artworkButton.upState = defaultCoverImg;
            artworkButton.downState = defaultCoverImg;
            artworkButton.overState = defaultCoverImg;
            artworkButton.hitTestState = defaultCoverImg;
            
            playButton.visible = true;
            pauseButton.visible = false;

            trackNameLabel.text = "";
            artistNameLabel.text = "";

            currentPlayback = 0;
            currentDuration = 0;
            dropStart = -1;
            
            isArtworkLoaded = false;
            isWaveformLoaded = false;
            useProgressBar = false;
        }
        
        public function attach():void {

            defaultCoverImg = new DefaultCoverAsset(); 
            defaultCoverImg.smoothing = true;
            artworkButton = new SimpleButton(defaultCoverImg, defaultCoverImg, 
                defaultCoverImg, defaultCoverImg);
            artworkButton.addEventListener(MouseEvent.CLICK, onCoverClicked);
            artworkButton.width = 136;
            artworkButton.height = 136;
            addChild(artworkButton);
            
            progressHandleImg = new ProgressHandleAsset();
            progressHandleImg.smoothing = true
            progressHandleImg.width = 24;
            progressHandleImg.height = 24;
            
            waveformSprite = new Sprite();
            waveformSprite.useHandCursor = true;
            waveformSprite.buttonMode = true;
            waveformSprite.tabEnabled = false;
            waveformSprite.addChild(progressHandleImg);
            
            waveformSprite.addEventListener(MouseEvent.MOUSE_DOWN, onWaveformMouseDown);
            waveformSprite.addEventListener(MouseEvent.MOUSE_OVER, onWaveformMouseEnter);
            waveformSprite.addEventListener(MouseEvent.MOUSE_OUT, onWaveformMouseLeave);
            
            stage.addEventListener(MouseEvent.MOUSE_UP, onWaveformMouseUp);
            stage.addEventListener(MouseEvent.MOUSE_MOVE, onWaveformMouseMove);

            addChild(waveformSprite);
            
            waveformData = new Array();
            downsizedWaveformData = new Array();


            // Text formats
            artistNameFormat = new TextFormat("Helvetica Neue", 12, ARTISTNAME_COLOR);
            artistNameFormat.bold = false;
            
            trackNameFormat = new TextFormat("Helvetica Neue", 14, TRACKNAME_COLOR);
            
            playbackFormat = new TextFormat("Helvetica Neue", 10, PLAYBACK_COLOR);
            durationFormat = new TextFormat("Helvetica Neue", 10, DURATION_COLOR);
            
            progressBarTimeFormat = new TextFormat("Helvetica Neue", 10, WAVEFORM_HOVER_PLAYBACK_COLOR);


            // Text field setup
            artistNameLabel = new TextField();
            artistNameLabel.text = "";
            artistNameLabel.textColor = ARTISTNAME_COLOR;
            artistNameLabel.setTextFormat(artistNameFormat);
            artistNameLabel.autoSize = TextFieldAutoSize.LEFT;
            artistNameLabel.visible = false;
            addChild(artistNameLabel);

            trackNameLabel = new TextField();
            trackNameLabel.text = "";
            trackNameLabel.textColor = TRACKNAME_COLOR;
            trackNameLabel.autoSize = TextFieldAutoSize.LEFT;
//            trackNameLabel.wordWrap = true;
            trackNameLabel.setTextFormat(trackNameFormat);
            trackNameLabel.visible = false;
            addChild(trackNameLabel);
            
            playbackLabel = new TextField();
            playbackLabel.text = "00:00";
            playbackLabel.textColor = PLAYBACK_COLOR;
            playbackLabel.autoSize = TextFieldAutoSize.LEFT;
            playbackLabel.setTextFormat(playbackFormat);
            playbackLabel.background = true;
            playbackLabel.backgroundColor = PLAYBACK_BG_COLOR;
            addChild(playbackLabel);

            durationLabel = new TextField();
            durationLabel.text = "00:00";
            durationLabel.textColor = DURATION_COLOR;
            durationLabel.autoSize = TextFieldAutoSize.LEFT;
            durationLabel.setTextFormat(durationFormat);
            durationLabel.background = true;
            durationLabel.backgroundColor = PLAYBACK_BG_COLOR;
            addChild(durationLabel);
            
            
            // Button setup
            playButton = new SimpleButton(
                new PlayNormalAsset(),
                new PlayHoverAsset(),
                new PlayDownAsset(),
                new PlayNormalAsset());
            playButton.addEventListener(MouseEvent.CLICK, onPlayBtnClicked);
            playButton.visible = true;
            playButton.width = 38;
            playButton.height = 38;
            addChild(playButton);

            pauseButton = new SimpleButton(
                new PauseNormalAsset(),
                new PauseHoverAsset(),
                new PauseDownAsset(),
                new PauseNormalAsset());
            pauseButton.addEventListener(MouseEvent.CLICK, onPauseBtnClicked);
            pauseButton.visible = false;
            pauseButton.width = 38;
            pauseButton.height = 38;
            addChild(pauseButton);
            
            shareNormalImg = new ShareNormalAsset();
            shareHoverImg = new ShareHoverAsset();
            shareDownImg = new ShareDownAsset();
            shareSelectedImg = new ShareSelectedAsset();

            shareButton = new SimpleButton(
                shareNormalImg,
                shareHoverImg,
                shareDownImg,
                shareNormalImg);
            shareButton.width = 56;
            shareButton.height = 21;
            shareButton.addEventListener(MouseEvent.CLICK, onShareBtnClicked);
            addChild(shareButton);

            logoButton = new SimpleButton(
                new LogoNormalAsset(),
                new LogoHoverAsset(),
                new LogoHoverAsset(),
                new LogoNormalAsset());
            logoButton.width = 81;
            logoButton.height = 11;
            logoButton.addEventListener(MouseEvent.CLICK, onLogoBtnClicked);
            addChild(logoButton);

            stage.addEventListener(Event.RESIZE, onResize);
            onResize(null);
        }

        public function onShareBtnClicked(e:MouseEvent):void {
            if (listener.onShareBtnClicked()) {
                toggleShareBtn();
            }
        }
        
        public function toggleShareBtn():void {
            isShareBtnSelected = !isShareBtnSelected;
            if (isShareBtnSelected) {
                shareButton.upState = shareSelectedImg;
                shareButton.downState = shareSelectedImg;
                shareButton.overState = shareSelectedImg;
            } else {
                shareButton.upState = shareNormalImg;
                shareButton.downState = shareDownImg;
                shareButton.overState = shareHoverImg;
            }
        }
        
        public function onLogoBtnClicked(e:MouseEvent):void {
            var url:URLRequest = new URLRequest("http://dropbeat.net");
            navigateToURL(url, "_blank");
        }
        
        public function onWaveformMouseEnter(e:MouseEvent):void {
            mouseWaveformHover = true; 
            if (useProgressBar) {
                drawProgress();
            } else {
                drawWaveform();
            }

        }

        public function onWaveformMouseLeave(e:MouseEvent):void {
            mouseWaveformHover = false; 
            if (useProgressBar) {
                drawProgress();
            } else {
                drawWaveform();
            }
        }

        public function onWaveformMouseDown(e:MouseEvent):void {
            mouseWaveformDragging = true; 
            var x = e.stageX - waveformSprite.x;
            var w = waveformSprite.width;
            var percent = Math.max(Math.min(x * 100 / w, 100), 0);
            if (useProgressBar) {
                drawProgress(percent);
            } else {
                drawWaveform(percent);     
            }

            if (currentDuration > 0) {
                playbackLabel.text = toTimeFormat(Math.round(currentDuration * percent / 100));
                playbackLabel.setTextFormat(useProgressBar ? 
                    progressBarTimeFormat : playbackFormat);
            }
        }

        public function onWaveformMouseUp(e:MouseEvent):void {
            if (!mouseWaveformDragging) {
                return;
            }
            mouseWaveformDragging = false; 

            var x = e.stageX - waveformSprite.x;
            var w = waveformSprite.width;
            var percent = Math.max(Math.min(x * 100 / w, 100), 0);
            listener.onSeek(percent);
        }
        
        public function onWaveformMouseMove(e:MouseEvent):void {
            if (!mouseWaveformDragging) {
                return;
            }
            var x = e.stageX - waveformSprite.x;
            var w = waveformSprite.width;
            var percent = Math.max(Math.min(x * 100 / w, 100), 0);
            if (useProgressBar) {
                drawProgress(percent);
            } else {
                drawWaveform(percent);     
            }

            if (currentDuration > 0) {
                playbackLabel.text = toTimeFormat(Math.round(currentDuration * percent / 100));
                playbackLabel.setTextFormat(useProgressBar ? 
                    progressBarTimeFormat : playbackFormat);
            }
        }
        
        public function onCoverClicked(e:MouseEvent):void {
            if (linkUrl != null) {
                var url:URLRequest = new URLRequest(linkUrl);
                navigateToURL(url, "_blank");
            }
        }
        
        public function onPlayBtnClicked(e:MouseEvent):void {
            listener.onPlayBtnClicked();
        }

        public function onPauseBtnClicked(e:MouseEvent):void {
            listener.onPauseBtnClicked()
        }
        
        public function setPlayer(soundPlayer:BasePlayer):void {
            this.soundPlayer = soundPlayer;
        }
            
        public function updatePlayInfoWithUserTrack(track:UserTrack, url:String):void {
            linkUrl = url;
            artworkWidth = 136;
            artworkButton.visible = true;
            setImage(track.getArtwork()); 
            
            artistName = track.getUserNickname();
            artistNameLink = "http://dropbeat.net/r/" + track.getUserResourceName() + "/";
            artistNameLabel.htmlText = 
                "<a target=\"_blank\" href=\"" + artistNameLink + "\">" + artistName + "</a>";
            artistNameLabel.visible = true;
            artistNameLabel.setTextFormat(artistNameFormat);
            
            trackName = track.getTrackName();
            trackNameLink = url;
            trackNameLabel.htmlText = "<a target=\"_blank\" href=\"" + trackNameLink + "\">" + trackName + "</a>";
            trackNameLabel.setTextFormat(trackNameFormat);
            trackNameLabel.visible = true;
            
            setWaveform(track.getWaveformUrl());
            
            dropStart = track.getDropStart();

            onResize(null);
        }
        
        public function updatePlayInfoWithExternalTrack(track:ExternalTrack, url:String):void {
            linkUrl = url;
//            if (track.getType() == "youtube") {
////                artworkWidth = 213;
////                setImage(null);
//            } else {
//                artworkButton.visible = true;
//                artworkWidth = 120;
//                setImage(track.getArtwork());
//            }
            artworkButton.visible = true;
            artworkWidth = 136;
            setImage(track.getArtworkUrl());
            
            artistNameLabel.visible = false;
            artistName = "";
            artistNameLink = "";

            trackName = track.getTitle();
            trackNameLink = url;
            trackNameLabel.htmlText = "<a href=\"" + trackNameLink + "\">" + trackName + "</a>";
            trackNameLabel.setTextFormat(trackNameFormat);
            trackNameLabel.visible = true;

            setWaveform(track.getWaveformUrl());

            dropStart = -1;

            onResize(null);
        }
        
        public function drawProgress(dragPercent:Number=-1):void {

            var view:Sprite = waveformSprite;
            
            var stageWidth = stage.stageWidth;
            var stageHeight = stage.stageHeight;
            
            view.x = 7 + artworkWidth + 10;
            var width = stageWidth - (7 + artworkWidth + 10) - 10 - 15;

            view.y = 140 - 18 - 30;
            view.height = 24;
            view.width = width + 20;

            var graphics:Graphics = waveformSprite.graphics;
            graphics.clear();
            
            graphics.beginFill(BG_COLOR);
            graphics.drawRect(0, 0, width + 20, 24);
            graphics.endFill();
            
            var playbackToX:Number;
            
            if (dragPercent == -1) {
                playbackToX = currentDuration <= 0 ? 0 : 
                    Math.round((currentPlayback / currentDuration) * width);
            } else {
                playbackToX = Math.round((dragPercent / 100) * width);
            }
            var dropFromX:Number = dropStart > -1 && currentDuration > 0 ?
                    Math.round(dropStart * 1000 * width / currentDuration) : -1;
            var dropToX:Number = dropStart > -1 && currentDuration > 0 ? 
                    Math.round((dropStart + 20) * 1000 * width / currentDuration) : -1;


            var playbackColor = mouseWaveformHover || mouseWaveformDragging ? 
                0x5c5c5c : 0x6e6e6e;

            var totalColor = mouseWaveformHover || mouseWaveformDragging ?
                WAVEFORM_HOVER_TOTAL_COLOR : WAVEFORM_TOTAL_COLOR;

            var dropColor = mouseWaveformHover || mouseWaveformDragging ?
                WAVEFORM_HOVER_DROP_COLOR : WAVEFORM_DROP_COLOR;

            var dropPlaybackColor = mouseWaveformHover || mouseWaveformDragging ? 
                WAVEFORM_HOVER_DROP_WITH_PLAYBACK_COLOR : WAVEFORM_DROP_WITH_PLAYBACK_COLOR;
            
            graphics.beginFill(totalColor);
            graphics.drawRoundRect(10, 9, width, 4, 2, 2); 
            graphics.endFill();
            
            graphics.beginFill(playbackColor);
            graphics.drawRoundRect(10, 9, playbackToX, 4, 2, 2); 
            graphics.endFill();
            
            graphics.beginFill(dropColor);
            graphics.drawRoundRect(10 + dropFromX, 9, dropToX - dropFromX, 4, 2, 2); 
            graphics.endFill();
        
            progressHandleImg.visible = true;
            progressHandleImg.x = 10 + playbackToX - 12;
            progressHandleImg.y = 0;
        }
            
        
        public function updateWaveform():void {
            if (waveformData.length == 0) {
                waveformSprite.graphics.clear();
                return;
            }

            downsizedWaveformData = new Array();
            
            var agg:Number = 0; 
            var count:int = 0;
            var prevBarIdx:int = 0;
            var barIdx:int = 0;
            var width:int = stage.stageWidth - (2 + artworkWidth + 10) - 10;
            var binCount:int = Math.ceil((width + BIN_MARGIN) / (BIN_WIDTH + BIN_MARGIN));
            var len:int = waveformData.length;
            var maxValue:Number = -1;
            for (var i:int = 0; i < len; i++) {
                barIdx = Math.floor(i * binCount / len);
                if (prevBarIdx != barIdx) {
                    var val:Number = agg / count;
                    if (maxValue < val) {
                        maxValue = val;
                    }
                    downsizedWaveformData.push(val);
                    agg = 0;
                    count = 0;
                    if (i == len) {
                        break;
                    }
                }
                prevBarIdx = barIdx;
                agg += waveformData[i];
                count += 1;
            }
            for (var i:int = 0; i < binCount; i++) {
                downsizedWaveformData[i] = (int)(downsizedWaveformData[i] * 100 / maxValue);
            }

            drawWaveform();
        }
        
        public function drawWaveform(dragPercent:Number=-1):void {
            
            var view:Sprite = waveformSprite;

            if (downsizedWaveformData.length == 0) {
                return;
            }

            progressHandleImg.visible = false;
            
            var stageWidth = stage.stageWidth;
            var stageHeight = stage.stageHeight;
            
            var playbackColor = mouseWaveformHover || mouseWaveformDragging ? 
                WAVEFORM_HOVER_PLAYBACK_COLOR : WAVEFORM_PLAYBACK_COLOR;

            var totalColor = mouseWaveformHover || mouseWaveformDragging ?
                WAVEFORM_HOVER_TOTAL_COLOR : WAVEFORM_TOTAL_COLOR;

            var dropColor = mouseWaveformHover || mouseWaveformDragging ?
                WAVEFORM_HOVER_DROP_COLOR : WAVEFORM_DROP_COLOR;

            var dropPlaybackColor = mouseWaveformHover || mouseWaveformDragging ? 
                WAVEFORM_HOVER_DROP_WITH_PLAYBACK_COLOR : WAVEFORM_DROP_WITH_PLAYBACK_COLOR;

            view.x = 2 + artworkWidth + 10;
            var width = stageWidth - (2 + artworkWidth + 10) - 10;

            view.y = 140 - BIN_HEIGHT - 10;
            view.height = BIN_HEIGHT;
            view.width = width;
            
            var playbackToX:Number;
            
            if (dragPercent == -1) {
                playbackToX = currentDuration <= 0 ? 0 : 
                    Math.round((currentPlayback / currentDuration) * width);
            } else {
                playbackToX = Math.round((dragPercent / 100) * width);
            }
            var dropFromX:Number = dropStart > -1 && currentDuration > 0 ?
                    Math.round(dropStart * 1000 * width / currentDuration) : -1;
            var dropToX:Number = dropStart > -1 && currentDuration > 0 ? 
                    Math.round((dropStart + 20) * 1000 * width / currentDuration) : -1;
            
            var graphics:Graphics = view.graphics;
            graphics.clear();

            graphics.beginFill(BG_COLOR);
            graphics.drawRect(0, 0, width, BIN_HEIGHT);
            graphics.endFill();

            for (var i:int = 0; i < downsizedWaveformData.length; i++) {
                var val:Number = downsizedWaveformData[i];
                if (isNaN(val)) {
                    break;
                }
                var x:Number = i * (BIN_WIDTH + BIN_MARGIN);
                var h:Number = BIN_HEIGHT * val / 100;
                var y:Number = BIN_HEIGHT - h; 
                var endBin:Number = x + BIN_WIDTH - 1;
                
                var isStartPlayback:Boolean = false;
                var isStartDrop:Boolean = false;

                var isEndPlayback:Boolean = false;
                var isEndDrop:Boolean = false;

                if (x < playbackToX) {
                    isStartPlayback = true;
                }

                if (endBin < playbackToX) {
                    isEndPlayback = true;
                }
                
                if (dropStart > - 1 && x < dropToX && x > dropFromX) {
                    isStartDrop = true;
                }

                if (dropStart > -1 && endBin < dropToX && endBin > dropFromX) {
                    isEndDrop = true;
                }
                

                var color:uint;

                if (isStartPlayback) {
                    color = playbackColor;
                    if (isStartDrop) {
                        color = dropPlaybackColor;
                    }
                } else {
                    color = totalColor;
                    if (isStartDrop) {
                        color = dropColor;
                    }
                }
                graphics.beginFill(color);

                if (isStartPlayback == isEndPlayback && isStartDrop == isEndDrop) {
                    graphics.drawRect(x, y, BIN_WIDTH, h);
                    graphics.endFill();
                    continue;
                }
                
                if (isStartPlayback == isEndPlayback) {
                    var p1:Number;
                    if (isStartDrop) {
                        p1 = dropToX;
                    } else {
                        p1 = dropFromX;
                    }
                    graphics.drawRect(x, y, p1 - x, h);
                    graphics.endFill();

                    color = isStartPlayback ? playbackColor : totalColor;

                    graphics.beginFill(color);
                    graphics.drawRect(p1, y, BIN_WIDTH - (p1 - x), h);
                    graphics.endFill();

                    continue;
                }
                
                if (isStartDrop == isEndDrop) {
                    var p1:Number = playbackToX;
                    graphics.drawRect(x, y, p1 - x, h);
                    graphics.endFill();

                    color = isStartDrop? dropColor : totalColor;

                    graphics.beginFill(color);
                    graphics.drawRect(p1, y, BIN_WIDTH - (p1 - x), h);
                    graphics.endFill();
                    continue;
                }

                graphics.drawRect(x, y, BIN_WIDTH, h);
                graphics.endFill();
                continue;
                
                var p1:Number;
                var p2:Number;
                var color2:uint;
                var color3:uint;

                if (isStartDrop) {
                    if (dropToX < playbackToX) {
                        p1 = dropToX; 
                        p2 = playbackToX;
                        color2 = playbackColor;
                        color3 = totalColor;
                    } else {
                        p1 = playbackToX; 
                        p2 = dropToX;
                        color2 = dropColor;
                        color3 = totalColor;
                    }
                } else {
                    if (dropFromX < playbackToX) {
                        p1 = dropFromX; 
                        p2 = playbackToX;
                        color2 = dropPlaybackColor;
                        color3 = dropColor;
                    } else {
                        p1 = dropFromX; 
                        p2 = dropFromX;
                        color2 = totalColor;
                        color3 = dropColor;
                    }
                }
                graphics.drawRect(x, y, Math.floor(p1 - x), h);
                graphics.endFill();
                
                graphics.beginFill(color2);
                graphics.drawRect(Math.floor(p1 - x) + x, y, Math.floor(p2 - p1), h);
                graphics.endFill();

                graphics.beginFill(color3);
                graphics.drawRect(Math.floor(p2 - p1) + Math.floor(p1 - x) + x, y, 
                    BIN_WIDTH - Math.floor(p2 - p1) - Math.floor(p1 - x), h);
                graphics.endFill();
            }
        }
        
        public function setImage(url:String):void {
            if (videoLoader != null) {
                removeChild(videoLoader);
                videoLoader.close();
                videoLoader = null;
            }
            if (artworkLoader != null) {
                artworkLoader.close();
                artworkLoader = null;
            }
            if (url != null) {
                Security.allowDomain("*");
                artworkLoader = new Loader();
                var artworkReq = new URLRequest(url);
                artworkLoader.load(artworkReq, new LoaderContext(true));
                var loaderInfo = artworkLoader.contentLoaderInfo;
                loaderInfo.addEventListener(Event.COMPLETE, onArtworkLoaded);
                loaderInfo.addEventListener(ErrorEvent.ERROR, onLoadError);
                loaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onLoadError);
                loaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onLoadError);
                
                function unbindEvent():void {
                    loaderInfo.removeEventListener(Event.COMPLETE, onArtworkLoaded);
                    loaderInfo.removeEventListener(ErrorEvent.ERROR, onLoadError);
                    loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onLoadError);
                    loaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onLoadError);
                }

                function onArtworkLoaded(e:Event):void {
                    unbindEvent();
                    try {
                        var bitmap:Bitmap = Bitmap(artworkLoader.content);
                        bitmap.smoothing = true;
                        artworkButton.upState = bitmap; 
                        artworkButton.downState = bitmap; 
                        artworkButton.overState = bitmap; 
                        artworkButton.hitTestState = bitmap; 
                    } catch (error:Error) {
                        artworkButton.upState = artworkLoader; 
                        artworkButton.downState = artworkLoader; 
                        artworkButton.overState = artworkLoader; 
                        artworkButton.hitTestState = artworkLoader; 
                    }
                    artworkButton.width = 136;
                    artworkButton.height = 136;

                    isArtworkLoaded = true;
                    mayControlReady();
                }
                
                function onLoadError(e:Event):void {
                    unbindEvent();
                    artworkButton.upState = defaultCoverImg; 
                    artworkButton.downState = defaultCoverImg; 
                    artworkButton.overState = defaultCoverImg; 
                    artworkButton.hitTestState = defaultCoverImg; 
                    artworkButton.width = 136;
                    artworkButton.height = 136;

                    isArtworkLoaded = true;
                    mayControlReady();
                }
            } else {
                artworkButton.upState = defaultCoverImg; 
                artworkButton.downState = defaultCoverImg; 
                artworkButton.overState = defaultCoverImg; 
                artworkButton.hitTestState = defaultCoverImg; 
                artworkButton.width = 136;
                artworkButton.height = 136;

                isArtworkLoaded = true;
                mayControlReady();
            }
        }
        
        public function setVideo(videoLoader: Loader):void {
            if (this.videoLoader != null) {
                removeChild(this.videoLoader);
                this.videoLoader = null;
            }
            if (artworkLoader != null) {
                artworkLoader.close();
                artworkLoader = null;
            }
            if (videoLoader != null) {

                this.videoLoader = videoLoader;
                videoLoader.width = 213;
                videoLoader.height = 120;
                addChild(videoLoader);
                onResize(null);

                artworkButton.upState = defaultCoverImg;
                artworkButton.downState = defaultCoverImg;
                artworkButton.overState = defaultCoverImg;
                artworkButton.hitTestState = defaultCoverImg;
                removeChild(artworkButton);
                addChild(artworkButton);
                artworkButton.alpha = 1;
                artworkButton.width = 120;
            } else {
                artworkButton.upState = defaultCoverImg;
                artworkButton.downState = defaultCoverImg;
                artworkButton.overState = defaultCoverImg;
                artworkButton.hitTestState = defaultCoverImg;
                artworkButton.alpha = 1;
                artworkButton.width = 120;
            }
        }
        
        public function setWaveform(waveformUrl:String):void {
            if (waveformUrl == null) {
                useProgressBar = true;

                playbackLabel.background = false;
                durationLabel.background = false;

                isWaveformLoaded = true;
                mayControlReady();
                return;
            }
            var request:Request = new Request(waveformUrl, URLRequestMethod.GET, null, 
                onComplete, onSecurityError, onIOError);
            request.send();

            function onComplete(data: Object):void {
                waveformData = data as Array;
                playbackLabel.background = true;
                durationLabel.background = true;
                useProgressBar = false;
                
                isWaveformLoaded = true;
                mayControlReady();
            }
            
            function onSecurityError(e:SecurityErrorEvent):void {
                useProgressBar = true;
                playbackLabel.background = false;
                durationLabel.background = false;

                isWaveformLoaded = true;
                mayControlReady();
            }

            function onIOError(e:IOErrorEvent):void {
                useProgressBar = true;
                playbackLabel.background = false;
                durationLabel.background = false;

                isWaveformLoaded = true;
                mayControlReady();
            }
        }
        
        public function setDuration(duration:int):void {
            if (duration <= 0) {
                return;
            }
            currentDuration = duration;

            durationLabel.text = toTimeFormat(duration);
            durationLabel.setTextFormat(useProgressBar ? 
                progressBarTimeFormat : durationFormat);
            if (useProgressBar) {
                drawProgress();
            } else {
                drawWaveform();
            }
        }

        public function detach():void {
            stage.removeEventListener(Event.RESIZE, onResize);
        }
        
        public function mayControlReady():void {
            if (!isArtworkLoaded || !isWaveformLoaded) {
                return;
            }
            listener.onPlayControlReady();
            onResize(null);
        }
        
        public function onShareLayerToggle(isVisible:Boolean):void {
            isShareBtnSelected = isVisible;
            if (isShareBtnSelected) {
                shareButton.upState = shareSelectedImg;
                shareButton.downState = shareSelectedImg;
                shareButton.overState = shareSelectedImg;
            } else {
                shareButton.upState = shareNormalImg;
                shareButton.downState = shareDownImg;
                shareButton.overState = shareHoverImg;
            }
        }
        
        public function onLogoClicked(e:MouseEvent):void {
            var url:URLRequest = new URLRequest("http://dropbeat.net");
            navigateToURL(url, "_blank");
        }
        
        public function onResize(e:Event):void {
            
            var stageWidth:Number = stage.stageWidth;
            var stageHeight:Number = stage.stageHeight;
            var stageAspectRatio = stageWidth / stageHeight;
            
            graphics.clear();
            graphics.beginFill(BG_COLOR);
            graphics.lineStyle(1, BORDER_COLOR);
            graphics.drawRect(0, 0, stageWidth - 1, stageHeight - 1);
            graphics.endFill();

            artworkButton.x = 2;
            artworkButton.y = 2;
            
            if (videoLoader) {            
                videoLoader.x = 2;
                videoLoader.y = 2;
            }
            
            playButton.x = 2 + artworkWidth + 10;
            playButton.y = 10;
            
            pauseButton.x = 2 + artworkWidth + 10;
            pauseButton.y = 10;
            
            var playButtonWidth = playButton.visible || pauseButton.visible ? 
                    playButton.width : 0;

          
            logoButton.x = stageWidth - 10 - logoButton.width;
            logoButton.y = 15;
            shareButton.x = stageWidth - 10 - shareButton.width;
            shareButton.y = 33;
            
            var buttonWidth:int = Math.max(logoButton.width, shareButton.width) + 10;
                
            trackNameLabel.x = playButton.x + playButtonWidth + 5;

            if (artistNameLabel.visible) {
                artistNameLabel.x = playButton.x + playButtonWidth + 5;
                
                var headerHeight:Number = trackNameLabel.height + artistNameLabel.height;
                if (headerHeight > playButton.height) {
                    trackNameLabel.y = 10;
                    artistNameLabel.y = 10 + trackNameLabel.height + 0;
                } else {
                    trackNameLabel.y = 10 + (playButton.height - headerHeight) / 2;
                    artistNameLabel.y = trackNameLabel.y + trackNameLabel.height;
                }

                var artistNameWidth = stageWidth - artistNameLabel.x - buttonWidth -5;
                var a:String = artistName;
                artistNameLabel.htmlText = "<a target=\"_blank\" href=\"" + artistNameLink + "\">" + a + "</a>";
                artistNameLabel.setTextFormat(artistNameFormat);
                while(artistNameLabel.width > artistNameWidth && a.length > 3) {
                    a = a.substr(0, -4) + "...";
                    artistNameLabel.htmlText = "<a target=\"_blank\" href=\"" + artistNameLink + "\">" + a + "</a>";
                    artistNameLabel.setTextFormat(artistNameFormat);
                }
            } else {
                if (trackNameLabel.height > playButton.height) {
                    trackNameLabel.y = 10;
                } else {
                    trackNameLabel.y = 10 + (playButton.height - trackNameLabel.height) / 2;
                }
            }
            var trackNameWidth = stageWidth - trackNameLabel.x - buttonWidth -5;
            var t:String = trackName;
            trackNameLabel.htmlText = "<a target=\"_blank\" href=\"" + trackNameLink + "\">" + t + "</a>";
            trackNameLabel.setTextFormat(trackNameFormat);
            while(trackNameLabel.width > trackNameWidth && t.length > 3) {
                t = t.substr(0, -4) + "...";
                trackNameLabel.htmlText = "<a target=\"_blank\" href=\"" + trackNameLink + "\">" + t + "</a>";
                trackNameLabel.setTextFormat(trackNameFormat);
            }

            durationLabel.x = stageWidth - 15 - durationLabel.width;
            durationLabel.y = 110;
            
            if (useProgressBar) {
                playbackLabel.x = 12 + artworkWidth + 10 + 5;
            } else {
                playbackLabel.x = 2 + artworkWidth + 10 + 5;
            }
            playbackLabel.y = 110;
            
             
            if (useProgressBar) {
                drawProgress();
            } else {
                updateWaveform();
            }
        }
        
        public function onPlayStateChanged(state:int):void {
            switch(state) {
            case PlayState.IDEL:
                playButton.visible = true;
                pauseButton.visible = false;
                break; 
            case PlayState.LOADING:
                playButton.visible = true;
                pauseButton.visible = false;
                break;
            case PlayState.READY_TO_PLAY:
                playButton.visible = true;
                pauseButton.visible = false;
                break;
            case PlayState.PAUSED:
                playButton.visible = true;
                pauseButton.visible = false;
                break
            case PlayState.PLAYING:
                playButton.visible = false;
                pauseButton.visible = true;
                break;
            }
            onResize(null);
        }
        
        public function onTick(playback:int, duration:int):void {
            if (duration < 0) {
                duration = 0;
            }
            durationLabel.text = toTimeFormat(duration);
            durationLabel.setTextFormat(useProgressBar ?
                progressBarTimeFormat : durationFormat);

            
            if (duration == 0) {
                playbackLabel.text = toTimeFormat(playback);
                playbackLabel.setTextFormat(useProgressBar ? 
                    progressBarTimeFormat : playbackFormat);
//                progressBar.indeterminate = true;
            } else {
                currentPlayback = playback;
                currentDuration = duration;

                if (!mouseWaveformDragging) {
                    if (useProgressBar) {
                        drawProgress();
                    } else {
                        drawWaveform();
                    }

                    playbackLabel.text = toTimeFormat(playback);
                    playbackLabel.setTextFormat(useProgressBar ? 
                        progressBarTimeFormat : playbackFormat);
                }
//                progressBar.indeterminate = false;
//                progressBar.setProgress(Math.round(playback * 100/ duration), 100);
            }
        }
        
        private function toTimeFormat(time:int):String {
            var sec:int = Math.floor(time / 1000);
            var min:int = Math.floor(sec / 60);
            var hour:int = Math.floor(min / 60);
            sec = sec % 60;
            var str:String = (min < 10 ? "0" : "") + min + ":" + (sec < 10 ? "0" : "") + sec;
            if (hour > 0) {
                min = min % 60;
                str = (min < 10 ? "0" : "") + min + ":" + (sec < 10 ? "0" : "") + sec;
                str = (hour < 10 ? "0" : "") + hour + ":" + str;
            }
            return str;
        }
        
    }
}