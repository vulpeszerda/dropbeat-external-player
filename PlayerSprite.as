package  {
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.SecurityErrorEvent;
	import flash.external.ExternalInterface;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.system.Security;
	import flash.text.TextField;
	
	import fl.controls.ProgressBar;
	import fl.controls.ProgressBarMode;
	
	public class PlayerSprite extends Sprite 
            implements PlayerStateListener, PlayControlListener, ShareControlListener{

        private var soundPlayer:BasePlayer;
        
        private var controlLayer:ControlLayer;
        private var initLayer:InitLayer;
        private var failureLayer:FailureLayer;
        private var shareLayer:ShareLayer;

        private var isPlayControlReady:Boolean = false; 
        private var isPlayerReady:Boolean = false;
        private var isReadyFired = false;
        private var isAutoPlay = false;
        
        private var logLabel:TextField;
        private var playDetailActions:Array = new Array();
        private var isPlayLogFired:Boolean = false;
        
        private var currentTrackTitle:String;
        private var currentTrackUid:String;

		public function PlayerSprite() {

            Security.allowDomain("*");
            
            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;
            stage.addEventListener(Event.RESIZE, onResize);
            
            initLayer = new InitLayer();
            controlLayer = new ControlLayer(this);

            shareLayer = new ShareLayer(this);

            stage.addChild(controlLayer);
            stage.addChild(shareLayer);
            stage.addChild(initLayer);
            
            logLabel = new TextField();
            logLabel.width = stage.stageWidth;
            logLabel.height = stage.stageHeight;
            logLabel.visible = false;
            stage.addChild(logLabel);

            initLayer.attach();
            controlLayer.attach();
            shareLayer.attach();

            onResize(null);

            resolveTrack();
		}
        
        public function log(message:String):void {
            logLabel.text = logLabel.text + "\n" + message;
        }
        
        private function reset() {
            isPlayControlReady = false;
            isPlayerReady = false;
            isReadyFired = false;
            soundPlayer.stop();
            soundPlayer = null;
            
            if (failureLayer != null) {
                removeChild(failureLayer);
                failureLayer = null;
            }
            initLayer.visible = true;
            controlLayer.visible = false;
        }
        
        private function resolveTrack() {
            var resolveUrl:String;
            var debug:Boolean;
			
            var globalParam = this.root.loaderInfo.parameters;

            for (var el:String in globalParam) {
                if (el == "url") {
                    resolveUrl = unescape(globalParam[el] as String);	
                }
                if (el == "autoplay") {
                    isAutoPlay = unescape(globalParam[el] as String) == "true";
                }
                if (el == "dbt_debug") {
                    debug = unescape(globalParam[el] as String) == "true";
                }
            }
            
            Path.init(debug);

			if (resolveUrl == null) {
                showError("Invalid track url.");
				return;
			}

			var idx:int = resolveUrl.indexOf(Path.HOST_NAME);
			if (idx < 0) {
                showError("Invalid track url.");
                return;
			}
			var path:String = resolveUrl.substring(idx + Path.HOST_NAME.length);
			if (path.substr(0,1) == "/") {
				path = path.substr(1, path.length - 1);
			}
			var subPaths:Array = path.split("/");
			
        	if (subPaths.length < 2) {
				// reoslve with key
				var partial:Array = resolveUrl.split("?");
				if (partial.length < 2) {
                    showError("Invalid track url.");
					return;
				}
				var allParams = partial[1].split("&");
				var sharedTrackKey:String;

				for each (var param:String in allParams) {
					var keyval:Array = param.split("=");
					if (keyval.length < 2) {
						continue;
					}
					var key:String = keyval[0];
					var val:String = keyval[1];

					if (key == "track") {
						sharedTrackKey = val;
						break;
					}
				}
				if (sharedTrackKey == null) {
                    showError("Invalid track url.");
					return;
				}
				// resolve with uid
				resolveSharedTrack(sharedTrackKey);
			} else {
				// resolve with url
				if (subPaths[0] != "r") {
                    showError("Invalid track url.");
					return;
				}
				resolveUserTrack(resolveUrl);
			}
        }
		
		private function resolveUserTrack(url:String):void {
            var that:PlayerStateListener = this;
            var param:URLVariables = new URLVariables();
            param["url"] = url;
            var request = new Request(Path.getURL(Path.RESOLVE), URLRequestMethod.GET, param, 
                onComplete, onSecurityError, onIOError);

            function onComplete(data: Object):void {
                if (!data.success) {
                    showError("Failed to fetch track info.");
                    return;
                }
                var track:UserTrack = new UserTrack(data.data);
                
                currentTrackTitle = track.getUserNickname() + " - " + track.getTrackName(); 
                currentTrackUid = track.getUniqueKey(); 
                
                var streamUrl = track.getStreamUrl();
                if (soundPlayer != null) {
                    soundPlayer.stop();
                }
                var wavExt = ".wav";
                if (streamUrl.lastIndexOf(wavExt) == (streamUrl.length - wavExt.length)) {
                    soundPlayer = new WavPlayer();
                    trace("loading with wavPlayer");
                } else {
                    soundPlayer = new MP3Player();
                    trace("loading with mp3Player");
                }
                soundPlayer.setListener(that);
                soundPlayer.load(streamUrl, track.getDuration() * 1000);
                
                controlLayer.setPlayer(soundPlayer);
                controlLayer.updatePlayInfoWithUserTrack(track, url);
                shareLayer.setShareInfo(
                    track.getUserNickname() + " - " + track.getTrackName(), url);
            }

            request.send();
		}
		
		private function resolveSharedTrack(key:String):void {
            var that:PlayerStateListener = this;
            var param:URLVariables = new URLVariables();
            param["uid"] = key;
            var request = new Request(Path.getURL(Path.SHARED_TRACK), URLRequestMethod.GET, param, 
                onComplete, onSecurityError, onIOError);
            request.send();

            function onComplete(data: Object):void {
                if (!data.success) {
                    showError("Failed to fetch track info.");
                    return;
                }
                var trackObj = data.data;
                var track:ExternalTrack = new ExternalTrack(trackObj.ref, trackObj.type, trackObj.track_name);

                currentTrackTitle = track.getTitle(); 
                currentTrackUid = track.getId(); 
                
                if (soundPlayer != null) {
                    soundPlayer.stop();
                }
                if (track.getType() == "youtube") {
                    soundPlayer = new YoutubePlayer();                
                    soundPlayer.setListener(that);
                    soundPlayer.load(track.getId());
                    trace("loading with youtubePlayer");
                    initControl(soundPlayer, track, key);
                } else if (track.getType() == "soundcloud") {
                    soundPlayer = new MP3Player();
                    soundPlayer.setListener(that);

                    var request:Request = new Request(Path.getURL(Path.META), URLRequestMethod.GET, null, 
                        onMetaReqComplete, onSecurityError, onIOError);
                    request.send();
                    function onMetaReqComplete(data:Object):void {
                        if (!data.success) {
                            showError("Failed to fetch resolve info.");
                            return;
                        }
                        var scKey:String = data.soundcloud_key;
                        track.setArtworkUrl(Path.SC_IMAGE_RESOLVE_URL + "?uid=" + track.getId() + "&size=large");         

                        resolveScTrack(track.getId(), scKey, function(data:Object):void {
                            if (data) {
                                var artworkUrl:String = data.artwork_url || data.user.avatar_url;
                                artworkUrl.replace("large.jpg", "t300x300.jpg");
                                track.setArtworkUrl(artworkUrl);         
                                //track.setWaveformUrl(Path.SC_WAVEFORM_URL + "?url=" + data.waveform_url);
                                track.setDuration(data.duration);
                                soundPlayer.load("https://api.soundcloud.com/tracks/" + 
                                    track.getId() + "/stream?consumer_key=" + scKey, data.duration);
                            } else {
                                soundPlayer.load("https://api.soundcloud.com/tracks/" + 
                                    track.getId() + "/stream?consumer_key=" + scKey);
                            }
                            
                            trace("loading with mp3Player");
                            initControl(soundPlayer, track, key);
                        });
                    }
                    
                } else if (track.getId().indexOf("http://") == 0) {
                    soundPlayer = new MP3Player();
                    soundPlayer.setListener(that);
                    soundPlayer.load(track.getId());
                    trace("loading with mp3Player");
                    initControl(soundPlayer, track, key);
                } else {
                    showError("Invalid track type.");
                    return;
                }
            }
            
            function initControl(player:BasePlayer, track:ExternalTrack, key:String):void {
                controlLayer.setPlayer(soundPlayer);
                controlLayer.updatePlayInfoWithExternalTrack(track, Path.getURL("") + "/?track=" + key);

                shareLayer.setShareInfo(track.getTitle(), Path.getURL("") + "/?track=" + key);
            }

		}
        
        private function resolveScTrack(id:String, key:String, onComplete:Function):void {
            var url:String = Path.SC_RESOLVE_URL + id + "?client_id=" + key;
            var request:Request = new Request(url, URLRequestMethod.GET, null, 
                    onComplete, _onSecurityError, _onIOError);
                    
            function _onSecurityError(e:SecurityErrorEvent):void {
                onComplete(null);
            }
            
            function _onIOError(e:IOErrorEvent):void {
                onComplete(null);
            }

            request.send();
        }
       
        private function getReferer():String {
			if(ExternalInterface.available) {
				try {	
                    Security.allowDomain("*");
					ExternalInterface.marshallExceptions = true; 
                	var url:String = ExternalInterface.call("window.location.href.toString");
                    return url;
				} catch(error:Error) {
                    return null;
				}
			}
            return null;
        }
        
        private function onSecurityError(e:SecurityErrorEvent):void {
            showError("Failed to load track. Security error");
        }
        
        private function onIOError(e:IOErrorEvent):void {
            showError("Failed to load track.");
        }
        
        public function onResize(e:Event):void {
            logLabel.width = stage.stageWidth;
            logLabel.height = stage.stageHeight; 
        }
        
        private function showError(message:String) {
            if (failureLayer != null) {
                removeChild(failureLayer);
                failureLayer = null;
            }
            controlLayer.visible = false;
            initLayer.visible = false;
            failureLayer = new FailureLayer(message);
            stage.addChild(failureLayer);
            failureLayer.attach();
        }
        
        public function onPlayStateChanged(state:int):void {
            if (state == PlayState.READY_TO_PLAY || state == PlayState.IDEL) {
                isPlayLogFired = false;
            } else if (state == PlayState.PLAYING && !isPlayLogFired) {
                logPlay(currentTrackUid, currentTrackTitle);
                isPlayLogFired = true;
            }
            controlLayer.onPlayStateChanged(state);
        }
        
        public function onFailure(e:Event):void {
            showError("Failed to play track.");
        }
        
        public function onTick():void {
            if (soundPlayer == null) {
                return;
            }
            var playback:int = soundPlayer.getPlayback();
            var duration:int = soundPlayer.getDuration();
            controlLayer.onTick(playback, duration);
        }
        
        public function onViewReadyToBind(loader:Loader):void {
            controlLayer.setVideo(loader);
        }
        
        public function onViewReadyToUnbind(loader:Loader):void {
            controlLayer.setVideo(null);
        }
        
        public function onPlayBtnClicked():Boolean {
            if (soundPlayer == null) {
                return false;
            }
            return soundPlayer.play();
        }
        
        public function onPauseBtnClicked():Boolean {
            if (soundPlayer == null) {
                return false;
            }
            return soundPlayer.pause();
        }

        public function onSeek(percent:Number):Boolean {
            if (soundPlayer == null) {
                return false;
            }
            
            var duration = soundPlayer.getDuration();
            if (duration <= 0) {
                return false; 
            }
            var seekTime:int = Math.max(duration * Math.min(percent, 100) / 100, 0);
            return soundPlayer.seek(seekTime);
        }
        
        public function onShareBtnClicked():Boolean {
            var value:Boolean = shareLayer.toggle(); 
            onResize(null);
            return value;
        }
		
        public function onPlayControlReady():void {
            trace("playerControl ready");
            isPlayControlReady = true; 
            mayPlayerReady();
        }
        
        public function onPlayerReady():void {
            trace("player ready");
            isPlayerReady = true; 
            mayPlayerReady();
        }
        
        public function onShareLayerToggle(isVisible:Boolean):void {
            controlLayer.onShareLayerToggle(isVisible);
        }
        
        private function mayPlayerReady():void {
            if (!isPlayControlReady || !isPlayerReady || soundPlayer == null || isReadyFired) {
                return;
            }
            if (isAutoPlay && !soundPlayer.play()) {
                return;
            }
            isReadyFired = true;

            initLayer.visible = false;
            controlLayer.visible = true;
            controlLayer.setDuration(soundPlayer.getDuration());

            if (soundPlayer.getType() == "youtube") {
                var player:YoutubePlayer = soundPlayer as YoutubePlayer;
                controlLayer.setVideo(player.getVideoView());
            }
            trace("duration=" + soundPlayer.getDuration());
        }
        
        public function logPlay(uid:String, title:String):void {
            var param:URLVariables = new URLVariables();
            param["uid"] = uid;
            param["t"] = title;
            param["device_type"] = "embed";
            var request:Request = new Request(Path.getURL(Path.LOG_PLAY), 
                URLRequestMethod.GET, param, 
                onLogComplete, onLogSecurityError, onLogIOError);
            request.send();
        }
        
//        public function sendPlayDetailLog() {
//            if (playDetailActions.length == 0) {
//                return;
//            }
//             
//            var param:String = JSON.stringify(playDetailActions); 
//            var request:Request = new Request(
//                Constant.LOG_PLAYBACK_DETAIL_URL, 
//                URLRequestMethod.POST, param, 
//                onLogComplete, onLogSecurityError, onLogIOError);
//            request.send();
//            playDetailActions = new Array();
//        }
//
//        public function logDetailStart(uid:String, title:String):String {
//            if (playDetailActions.length > 0) {
//                sendPlayDetailLog();
//            }
//            playDetailActions = new Array();
//            var obj:Object = new Object();
//            obj.type = "start";
//            playDetailActions.push(obj);
//        }
//        
//        public function logDetailSeekFrom(seekFrom:int):String {
//            var obj:Object = new Object();
//            obj.type = "seek_from";
//            obj.ts = seekFrom;
//            playDetailActions.push(obj);
//        }
//        
//        public function logDetailSeekTo(seekTo:int):String {
//            var obj:Object = new Object();
//            obj.type = "seek_to";
//            obj.ts = seekTo;
//            playDetailActions.push(obj);
//        }
//        
//        public function logDetailExit():String {
//            if (this.soundPlayer == null) {
//                return;
//            }
//            var obj:Object = new Object();
//            obj.type = "exit";
//            obj.ts = int(Math.round(this.soundPlayer.getPlayback() / 1000));
//            playDetailActions.push(obj);
//            sendPlayDetailLog();
//        }
//        
//        public function logDetailEnd():String {
//            if (this.soundPlayer == null) {
//                return;
//            }
//            var obj:Object = new Object();
//            obj.type = "end";
//            playDetailActions.push(obj);
//            sendPlayDetailLog();
//        }
        
        public function onLogComplete(data: Object):void {
            trace("on log success"); 
        }
            
        public function onLogSecurityError(e:SecurityErrorEvent):void {
            trace("log play security error"); 
        }
            
        public function onLogIOError(e:IOErrorEvent):void {
            trace("log play ioerror"); 
        }
	}
}