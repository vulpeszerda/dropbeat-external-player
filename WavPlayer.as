package {
    import org.as3wavsound.WavSound;
    import org.as3wavsound.WavSoundChannel;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.ProgressEvent;
    import flash.net.URLRequest;
    import flash.net.URLLoader;
	import flash.utils.ByteArray;

    public class WavPlayer extends BasePlayer {
        var sound:WavSound;
        var soundChannel:WavSoundChannel;
        var pausePosition:int = -1;
        var duration:int = -1;
        
        public function WavPlayer() {
        }
        
        override public function load(url:String, duration:int=-1):Boolean {
            if (this.state != PlayState.IDEL) {
                return false;
            }
            var req:URLRequest = new URLRequest(url);
            var loader:URLLoader = new URLLoader();
            loader.dataFormat = "binary";
            loader.load(req);
            loader.addEventListener(Event.COMPLETE, onLoaded);
            loader.addEventListener(IOErrorEvent.IO_ERROR, onError);
            loader.addEventListener(ProgressEvent.PROGRESS, onProgress);
            return true;
        }
        
        override public function play():Boolean {
            if (this.state != PlayState.PAUSED || pausePosition < 0) {
                return false;
            }
            setState(PlayState.PLAYING);
            if (soundChannel != null) {
                soundChannel.removeEventListener(Event.SOUND_COMPLETE, onFinished);
                soundChannel.stop();
            }
            soundChannel = sound.play(pausePosition); 
            soundChannel.addEventListener(Event.SOUND_COMPLETE, onFinished);
            return true;
        }
        
        
        override public function pause():Boolean {
            if (this.state != PlayState.PLAYING || soundChannel == null) {
                return false;
            }
            setState(PlayState.PAUSED);
            if (soundChannel != null) {
                soundChannel.removeEventListener(Event.SOUND_COMPLETE, onFinished);
                soundChannel.stop();
            }
            pausePosition = soundChannel.position;
            return true;
        }

        override public function stop():Boolean {
            if (this.state == PlayState.IDEL) {
                return false; 
            }
            if (soundChannel != null) {
                soundChannel.stop();
                soundChannel.removeEventListener(Event.SOUND_COMPLETE, onFinished);
                soundChannel = null;
            }
            duration = -1;
            pausePosition = -1;
            setState(PlayState.IDEL);
            return true;
        }
        
        override public function seek(time:int):Boolean {
            if (this.state != PlayState.PLAYING || this.state != PlayState.PAUSED
                    || duration < 0 || soundChannel == null) {
                return false; 
            }
            if (time > duration) {
                time = duration;
            }
            if (this.state == PlayState.PLAYING) {
                soundChannel.stop();
                soundChannel.removeEventListener(Event.SOUND_COMPLETE, onFinished);
                soundChannel = sound.play(time);
                soundChannel.addEventListener(Event.SOUND_COMPLETE, onFinished);
            } else {
                pausePosition = time;  
            }
            return true;
        }
        
        override public function getPlayback():int {
            return soundChannel == null ? 0 : soundChannel.position;
        }
        
        override public function getDuration():int {
            return duration;
        }

        override public function getType():String {
            return "wav";
        }
        
        protected function onLoaded(e:Event):void {
            setState(PlayState.PLAYING);
            
            sound = new WavSound(e.target.data as ByteArray); 
            
            soundChannel = sound.play(); 
            soundChannel.addEventListener(Event.SOUND_COMPLETE, onFinished);
            duration = sound.length;
        }
        
        protected function onFinished():void {
            if (soundChannel != null) {
                soundChannel.stop();
                soundChannel.removeEventListener(Event.SOUND_COMPLETE, onFinished);
                soundChannel = null;
            }
            duration = -1;
            pausePosition = -1;
            setState(PlayState.IDEL);
        }
        
        protected function onError(e:IOErrorEvent):void {
            trace ("io error"); 
            if (soundChannel != null) {
                soundChannel.stop();
                soundChannel.removeEventListener(Event.SOUND_COMPLETE, onFinished);
                soundChannel = null;
            }
            duration = -1;
            pausePosition = -1;
            setState(PlayState.IDEL);
        }
        
        protected function onProgress(e:ProgressEvent):void {
            trace (e.bytesLoaded + " / " + e.bytesTotal);
        }
    }
}