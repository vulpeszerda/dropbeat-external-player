package {
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.ProgressEvent;
    import flash.events.TimerEvent;
    import flash.media.Sound;
    import flash.media.SoundChannel;
    import flash.media.SoundLoaderContext;
    import flash.net.URLRequest;
    import flash.utils.Timer;

    public class MP3Player extends BasePlayer {
        private var sound:Sound;
        private var soundContext:SoundLoaderContext;
        private var soundChannel:SoundChannel;
        private var pausePosition:int = -1;
        private var duration:int = -1;
        private var hasAccurateDuration:Boolean = false;
        private var progressTimer:Timer;
        private var isFirstTick:Boolean = true;
        private var readyFired = false;

        public function MP3Player() {
            soundContext = new SoundLoaderContext(1000, false);
            sound = new Sound();
            sound.addEventListener(Event.COMPLETE, onLoaded);
            sound.addEventListener(IOErrorEvent.IO_ERROR, onError);
            sound.addEventListener(ProgressEvent.PROGRESS, onProgress);
        }
        
        override public function load(url:String, d:int=-1):Boolean {

            if (this.state != PlayState.IDEL) {
                return false;
            }
            setState(PlayState.LOADING);
            hasAccurateDuration = d > -1;

            duration = d;

            var req:URLRequest = new URLRequest(url);
            sound.load(req, soundContext);

            return true;
        }
        
        override public function play():Boolean {
            if (this.state != PlayState.READY_TO_PLAY && 
                (this.state != PlayState.PAUSED || pausePosition < 0)) {
                return false;
            }
            setState(PlayState.PLAYING);
            if (soundChannel != null) {
                soundChannel.removeEventListener(Event.SOUND_COMPLETE, onFinished);
                soundChannel.stop();
            }
            soundChannel = sound.play(
                this.state == PlayState.READY_TO_PLAY ? 0 : pausePosition); 
            soundChannel.addEventListener(Event.SOUND_COMPLETE, onFinished);

            if (progressTimer != null) {
                progressTimer.stop();
                progressTimer.removeEventListener(TimerEvent.TIMER, onTick);
            }

            progressTimer = new Timer(250);
            progressTimer.addEventListener(TimerEvent.TIMER, onTick);
            progressTimer.start();
            return true;
        }
        
        
        override public function pause():Boolean {
            if (this.state != PlayState.PLAYING || soundChannel == null) {
                return false;
            }
            setState(PlayState.PAUSED);
            if (soundChannel != null) {
                soundChannel.removeEventListener(Event.SOUND_COMPLETE, onFinished);
                soundChannel.stop();
            }
            pausePosition = soundChannel.position;

            if (progressTimer != null) {
                progressTimer.stop();
                progressTimer.removeEventListener(TimerEvent.TIMER, onTick);
            }
            return true;
        }

        override public function stop():Boolean {
            if (this.state == PlayState.IDEL) {
                return false; 
            }
            if (soundChannel != null) {
                soundChannel.stop();
                soundChannel.removeEventListener(Event.SOUND_COMPLETE, onFinished);
                soundChannel = null;
            }
            try {
                sound.close();
            } catch (e) {
                // do nothing                
            }
            pausePosition = -1;
            isFirstTick = true;
            setState(PlayState.IDEL);

            if (progressTimer != null) {
                progressTimer.stop();
                progressTimer.removeEventListener(TimerEvent.TIMER, onTick);
            }
            return true;
        }
        
        override public function seek(time:int):Boolean {
            if ((this.state != PlayState.PLAYING && this.state != PlayState.PAUSED)
                    || duration < 0 || soundChannel == null) {
                return false; 
            }
            if (time > duration) {
                time = duration;
            }
            if (this.state == PlayState.PLAYING) {
                soundChannel.stop();
                soundChannel.removeEventListener(Event.SOUND_COMPLETE, onFinished);
                soundChannel = sound.play(time);
                soundChannel.addEventListener(Event.SOUND_COMPLETE, onFinished);
            } else {
                pausePosition = time;  
            }
            
            return true;
        }
        
        override public function getPlayback():int {
            return soundChannel == null ? 0 : soundChannel.position;
        }
        
        override public function getDuration():int {
            return duration;
        }

        override public function getType():String {
            return "mp3";
        }
        
        protected function onLoaded(e:Event):void {
            duration = sound.length;
        }
        
        protected function onFinished(e:Event):void {
            stop();
            setState(PlayState.READY_TO_PLAY);
            pausePosition = 0;
        }
        
        protected function onError(e:IOErrorEvent):void {
            stop();
            listener.onFailure(e);
        }

        protected function onProgress(e:ProgressEvent):void {
            if (!hasAccurateDuration) {
                duration = sound.length / sound.bytesLoaded * sound.bytesTotal;
            }
            if (sound.bytesLoaded * 100 / sound.bytesTotal >= 1 && !readyFired) {
                readyFired = true;
                onLoadEnoughBuffer();
            }
        }
            
        protected function onLoadEnoughBuffer():void {
            setState(PlayState.READY_TO_PLAY);
            listener.onPlayerReady();
        }
            
        protected function onTick(e:Event) {
            if (soundChannel == null || duration < 0) {
                return;
            }
            if (isFirstTick) {
                setState(PlayState.PLAYING);                
                isFirstTick = false;
            }
            listener.onTick();
        }
        
    }
}