package {
    public class PlayState {
        public static const IDEL:int = 0;
        public static const LOADING:int = 1; 
        public static const READY_TO_PLAY:int = 2; 
        public static const PLAYING:int = 3;
        public static const PAUSED:int = 4;

        public function PlayState() {}
    }
}