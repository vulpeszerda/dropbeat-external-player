package {
    import flash.display.Loader;
    import flash.display.Sprite;
    import flash.net.URLRequest;
    
    public class ButtonImgDisplayState extends Sprite {
        
        public function ButtonImgDisplayState(url:String, _alpha:Number) {
            var my_loader : Loader = new Loader();
            my_loader.load(new URLRequest(url));
            addChild(my_loader);
            
            this.alpha = _alpha;
        }
    }
}