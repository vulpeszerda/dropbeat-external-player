package {
    public class Path {
        public static const HOST_NAME = "dropbeat.net";

        public static const DEBUG_HOST = "http://spark.coroutine.io";
        public static const HOST = "http://dropbeat.net";

        public static const API_PREFIX = "/api/v1";

        public static const RESOLVE = API_PREFIX + "/resolve/";
        public static const SHARED_TRACK = API_PREFIX + "/track/shared/";
        public static const META = API_PREFIX + "/meta/key/";

        public static const LOG_PLAY = API_PREFIX + "/log/play/";
        public static const LOG_PLAYBACK_DETAIL = API_PREFIX + "/log/playback_detail/";
        
        private static var debug:Boolean = false;
        
        public static function init(debug) {
            Path.debug = debug;
        }
        
        public static function getURL(path:String) {
            var host:String = Path.debug ? DEBUG_HOST : HOST;
            return host + path;
        }

        public static const SC_RESOLVE_URL = "http://api.soundcloud.com/tracks/";
        public static const SC_WAVEFORM_URL = "http://www.waveformjs.org/w";
        public static const SC_IMAGE_RESOLVE_URL = "http://core.dropbeat.net/api/image/soundcloud/";

        public function Path() {}
    }
}