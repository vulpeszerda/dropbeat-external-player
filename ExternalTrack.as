package  {
    public class ExternalTrack {
        private var id:String = "";
        private var type:String = "";
        private var title:String = "";
        private var waveformUrl:String = null;
        private var artworkUrl:String = null;
        private var duration:int = -1;

        public function ExternalTrack(id:String, type:String, title:String) {
            this.id = id;
            this.title = title;
            this.type = type;
        }
        
        public function getTitle():String {
            return title;
        }
        
        public function getId():String {
            return id;
        }
        
        public function getType():String {
            return type;
        }
        
        public function getArtworkUrl():String {
            if (artworkUrl != null) {
                return artworkUrl;
            }
            if (type == "youtube") {
                return "http://img.youtube.com/vi/" + id + "/default.jpg";
            }
            return "http://dropbeat.net/images/default_cover_big.png";
        }
        
        public function getWaveformUrl():String {
            return waveformUrl;
        }
        
        public function getDuration():int {
            return duration;
        }
        
        public function setWaveformUrl(url:String):void {
            this.waveformUrl = url;            
        }
        
        public function setArtworkUrl(url:String):void {
            this.artworkUrl = url;
        }
        
        public function setDuration(duration:int) {
            this.duration = duration;
        }
        
    }
}