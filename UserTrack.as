package  {

    public class UserTrack {
        private var id:int = -1;
        private var trackName:String = "";
        private var artwork:String = "";
        private var duration:int = -1;
        private var dropRef:String = "";
        private var dropStart:int = -1;
        private var streamUrl:String = "";
        private var waveformUrl:String = "";
        private var uniqueKey:String = "";
        private var trackType:String = "TRACK";
        private var userResourceName:String = "";
        private var userNickname:String = "";
        private var userProfileImage:String = "";
        private var likeCount:int = 0;
        private var downloadable:Boolean = false;
        private var genreId:int = -1;
        private var desc:String = "";
        private var resourceName:String = "";
        private var createdAt:String = "";

        public function UserTrack(object:Object) {
            this.id = object.id;
            this.trackName = object.name;
            this.artwork = object.coverart_url;
            this.duration = object.duration;
            this.dropRef = object.drop_url;
            this.dropStart = object.drop_start;
            this.streamUrl = object.stream_url;
            this.waveformUrl = object.waveform_url;
            this.uniqueKey = object.unique_key;
            this.trackType = object.track_type;
            this.userResourceName = object.user_resource_name;
            this.userNickname = object.user_name;
            this.userProfileImage = object.user_profile_image;
            this.likeCount = object.like_count;
            this.downloadable = object.downloadable;
            this.genreId = object.genre_id;
            this.desc = object.desc;
            this.createdAt = object.created_at;
        }
        
        public function getTrackName():String {
            return trackName;
        }
        
        public function getArtwork():String {
            return artwork;
        }
        
        public function getDuration():int {
            return duration;
        }
        
        public function getDropRef():String {
            return dropRef;
        }
        
        public function getDropStart():int {
            return dropStart;
        }
        
        public function getStreamUrl():String {
            return streamUrl;
        }
        
        public function getWaveformUrl():String {
            return waveformUrl;
        }

        public function getUniqueKey():String {
            return uniqueKey;
        }
        
        public function getTrackType():String {
            return trackType;
        }
        
        public function getUserResourceName():String {
            return userResourceName; 
        }
        
        public function getUserNickname():String {
            return userNickname;
        }
        
        public function getUserProfileImage():String {
            return userProfileImage;
        }
        
        public function getLikeCount():int {
            return likeCount; 
        }
        
        public function getDownloadable():Boolean {
            return downloadable;
        }
        
        public function getGenreId():int {
            return genreId;
        }
        
        public function getDesc():String {
            return desc;
        }
        
        public function getResourceName():String {
            return resourceName;
        }
        
        public function getCreatedAt():String {
            return createdAt; 
        }
    }
}