package {
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	
	import fl.controls.ProgressBar;
	import fl.controls.ProgressBarMode;

    public class InitLayer extends Sprite{
        private const BG_COLOR:uint = 0xf5f5f5;
        private const BORDER_COLOR:uint = 0xdddddd;
        
        [Embed(source='assets/bg_logo.png')]
        private var ImageAsset:Class;

        private var progressBar:ProgressBar;
        private var logoImg:Bitmap;

        public function InitLayer() {
        }
        
        public function attach():void {
            
            progressBar = new ProgressBar();
            progressBar.mode = ProgressBarMode.MANUAL;
            progressBar.maximum = 100;
            progressBar.minimum = 0;
            progressBar.indeterminate = true;
            progressBar.width = 140;
            progressBar.height = 5;
            addChild(progressBar);
            
            logoImg = new ImageAsset();
            logoImg.width = 60;
            logoImg.height = 60;
            addChild(logoImg);
            
            onResize(null);
            

            stage.addEventListener(Event.RESIZE, onResize);
        }
        
        public function detach():void {
            stage.removeChild(this);
            stage.removeEventListener(Event.RESIZE, onResize);
        }
        
        public function onResize(e:Event):void {
            
            var stageWidth:Number = stage.stageWidth;
            var stageHeight:Number = stage.stageHeight;

            graphics.beginFill(BG_COLOR);
            graphics.lineStyle(1, BORDER_COLOR);
            graphics.drawRect(0, 0, stageWidth - 1, stageHeight - 1);
            graphics.endFill();
            
            progressBar.x = (stageWidth - progressBar.width) / 2;
            progressBar.y = (stageHeight - 80) / 2 + 75;
            
            logoImg.x = (stageWidth - logoImg.width) / 2;
            logoImg.y = (stageHeight - 80) / 2;
        }
    }
}