package {
    public interface PlayControlListener {
        function onPlayBtnClicked():Boolean;
        function onPauseBtnClicked():Boolean;
        function onSeek(percent:Number):Boolean;
        function onShareBtnClicked():Boolean;
        function onPlayControlReady():void;
        function log(message:String):void;
    }
}