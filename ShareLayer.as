package {
    import flash.display.Graphics;
    import flash.display.SimpleButton;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.net.URLRequest;
    import flash.net.navigateToURL;
    import flash.text.TextField;
    import flash.text.TextFormat;

    public class ShareLayer extends Sprite{
        public static const BUBBLE_WIDTH:Number = 260;
        public static const BUBBLE_HEIGHT:Number = 95;

        private const FACEBOOK_APP_ID = "754181154701901";

        private const BUBBLE_BG_COLOR:uint = 0x525259;
        private const BUBBLE_INPUT_BG_COLOR:uint = 0x3f3f4a;
        private const TEXT_COLOR:uint = 0xffffff;

        [Embed(source='assets/bubble_arrow.png')]
        private var BubbleArrowAsset:Class;

        [Embed(source='assets/ic_facebook.png')]
        private var IconFacebookAsset:Class;

        [Embed(source='assets/ic_twitter.png')]
        private var IconTwitterAsset:Class;

        [Embed(source='assets/ic_tumblr.png')]
        private var IconTumblrAsset:Class;

        [Embed(source='assets/ic_email.png')]
        private var IconEmailAsset:Class;

        private var filter:Sprite;
        private var bubble:Sprite;
        private var bubbleImg;
        private var inputLabel:TextField;

        private var fbBtn:SimpleButton;
        private var twtBtn:SimpleButton;
        private var tbrBtn:SimpleButton;
        private var emailBtn:SimpleButton;
        
        private var shareUrl:String;
        private var trackName:String;
        
        private var inputLabelFormat:TextFormat;
        
        private var listener:ShareControlListener;


        public function ShareLayer(listener:ShareControlListener) {
            this.listener = listener;
        }

        public function attach():void {
            filter = new Sprite();
            filter.addEventListener(MouseEvent.CLICK, onFilterClicked);
            addChild(filter);

            bubble = new Sprite();
            addChild(bubble);
            
            bubbleImg = new BubbleArrowAsset();
            bubbleImg.width = 11;
            bubbleImg.height = 16;
            bubble.addChild(bubbleImg);
            
            inputLabel = new TextField();
            inputLabel.textColor = TEXT_COLOR;
            inputLabel.text = "";
            inputLabelFormat = new TextFormat("Helvetica Neue", 14, TEXT_COLOR);
            inputLabel.setTextFormat(inputLabelFormat);
            inputLabel.selectable = true;
            bubble.addChild(inputLabel);
            
            var fbIcon = new IconFacebookAsset();
            fbBtn = new SimpleButton();
            fbBtn.upState = fbIcon;
            fbBtn.downState = fbIcon;
            fbBtn.hitTestState = fbIcon;
            fbBtn.overState = fbIcon;
            fbBtn.addEventListener(MouseEvent.CLICK, onShareWithFacebook); 
            bubble.addChild(fbBtn);

            var twtIcon = new IconTwitterAsset();
            twtBtn = new SimpleButton();
            twtBtn.upState = twtIcon;
            twtBtn.downState = twtIcon;
            twtBtn.hitTestState = twtIcon;
            twtBtn.overState = twtIcon;
            twtBtn.addEventListener(MouseEvent.CLICK, onShareWithTwitter); 
            bubble.addChild(twtBtn);

            var tbrIcon = new IconTumblrAsset();
            tbrBtn = new SimpleButton();
            tbrBtn.upState = tbrIcon;
            tbrBtn.downState = tbrIcon;
            tbrBtn.hitTestState = tbrIcon;
            tbrBtn.overState = tbrIcon;
            tbrBtn.addEventListener(MouseEvent.CLICK, onShareWithTumblr); 
            bubble.addChild(tbrBtn);

            var emailIcon = new IconEmailAsset();
            emailBtn = new SimpleButton();
            emailBtn.upState = emailIcon;
            emailBtn.downState = emailIcon;
            emailBtn.hitTestState = emailIcon;
            emailBtn.overState = emailIcon;
            emailBtn.addEventListener(MouseEvent.CLICK, onShareWithEmail); 
            bubble.addChild(emailBtn);

            onResize(null);
            
            stage.addEventListener(Event.RESIZE, onResize);
            bubble.visible = false;
            filter.visible = false;
        }
        
        public function onFilterClicked(e:MouseEvent) {
            bubble.visible = false;
            filter.visible = false;
            listener.onShareLayerToggle(false);
        }
        
        public function setShareInfo(title:String, url:String):void {
            this.trackName = title;
            this.shareUrl = url;

            inputLabel.text = url;
            inputLabel.setTextFormat(inputLabelFormat);
        }
        
        public function onShareWithFacebook(e:MouseEvent):void {
            var _url:String = "https://www.facebook.com/dialog/share?app_id=" + FACEBOOK_APP_ID + 
                "&display=popup&href=" + shareUrl + "&redirect_uri=" + shareUrl;
            var url:URLRequest = new URLRequest(_url);
            navigateToURL(url, "_blank");
        }
        
        public function onShareWithTwitter(e:MouseEvent):void {
            var _url:String = "http://twitter.com/share?text=" + encodeURIComponent(trackName) +
                "&url=" + shareUrl + "&hashtags=DROPBEAT";
            var url:URLRequest = new URLRequest(_url);
            navigateToURL(url, "_blank");
        }
        
        public function onShareWithEmail(e:MouseEvent):void {
            var _url:String = "mailto:?subject=" + encodeURIComponent(trackName) +
                "&body=" + shareUrl;
            var url:URLRequest = new URLRequest(_url);
            navigateToURL(url, "_blank");
        }
        
        public function onShareWithTumblr(e:MouseEvent):void {
            var _url:String = "http://www.tumblr.com/share/audio" +
                "?externally_hosted_url=" + shareUrl + "&tags=" + encodeURIComponent(trackName);
            var url:URLRequest = new URLRequest(_url);
            navigateToURL(url, "_blank");
        }
        
        public function toggle():Boolean {
            bubble.visible = !bubble.visible;
            filter.visible = !filter.visible;
            onResize(null);
            return true; 
        }
        
        public function onResize(e:Event) {
            filter.x = 0;
            filter.y = 0;
            filter.width = stage.stageWidth;
            filter.height = stage.stageHeight;
            var filterGraphics:Graphics = filter.graphics;
            filterGraphics.beginFill(0xffffff);
            filterGraphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            filterGraphics.endFill();
            filter.alpha = 0;
            
            var bubbleX:Number = 10 + stage.stageWidth - 80 - ShareLayer.BUBBLE_WIDTH;
            var bubbleY:Number = 6;

            var graphics:Graphics = bubble.graphics;
            bubble.width = BUBBLE_WIDTH;
            bubble.height = BUBBLE_HEIGHT;
            bubble.x = bubbleX;
            bubble.y = bubbleY;

            graphics.clear();
            
            bubbleImg.x = ShareLayer.BUBBLE_WIDTH - 16;
            bubbleImg.y = 27;
            
            graphics.beginFill(BUBBLE_BG_COLOR);
            graphics.drawRoundRect(0, 0, 
                ShareLayer.BUBBLE_WIDTH - 16, ShareLayer.BUBBLE_HEIGHT, 4);
            graphics.endFill();
            
            var buttonFrameWidth:Number = 100;
            
            graphics.beginFill(BUBBLE_INPUT_BG_COLOR);
            graphics.drawRoundRect(10, 10, 
                ShareLayer.BUBBLE_WIDTH - 36, 30, 3);
            graphics.endFill();

            inputLabel.width = ShareLayer.BUBBLE_WIDTH - 46;
            inputLabel.height = 20;
            inputLabel.x = 20;
            inputLabel.y = 15;
            
            fbBtn.x = 10;
            fbBtn.y = 50;
            
            twtBtn.x = 50;
            twtBtn.y = 50;
            
            tbrBtn.x = 90;
            tbrBtn.y = 50;
            
            emailBtn.x = 130;
            emailBtn.y = 50;
        }
    }
}