package {
    import flash.display.Bitmap;
    import flash.display.SimpleButton;
    import flash.display.Sprite;
    import flash.display.Stage;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.net.URLRequest;
    import flash.net.navigateToURL;
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormat;
    import flash.text.TextFormatAlign;
    
    import fl.controls.Button;
    import fl.controls.ProgressBar;
    import fl.controls.ProgressBarMode;
    
    public class FailureLayer extends Sprite{
        private const BG_COLOR:uint = 0xf5f5f5;
        private const BORDER_COLOR:uint = 0xdddddd;
        private const TEXT_COLOR:uint = 0x88898c;
        
        [Embed(source='assets/logo_gray.png')]
        private var ImageAsset:Class;
        
        private var logoImg:Bitmap;
        private var logoButton:SimpleButton;
        private var message:String;
        private var titleLabel:TextField;
        private var messageLabel:TextField;
        
        public function FailureLayer(message:String) {
            this.message = message; 
        }
        
        public function attach():void {
            
            titleLabel = new TextField();
            titleLabel.text = "Failed to load track";
            titleLabel.textColor = TEXT_COLOR;
            titleLabel.autoSize = TextFieldAutoSize.LEFT;
            var titleFormat:TextFormat = new TextFormat("Helvetica Neue", 14, TEXT_COLOR);
            titleFormat.bold = true;
            titleLabel.setTextFormat(titleFormat);
            addChild(titleLabel);

            messageLabel = new TextField();
            messageLabel.text = message;
            messageLabel.textColor = TEXT_COLOR;
            messageLabel.autoSize = TextFieldAutoSize.CENTER;
            messageLabel.wordWrap = true;
            var messageFormat:TextFormat = new TextFormat("Helvetica Neue", 12, TEXT_COLOR);
            messageFormat.align = TextFormatAlign.CENTER;
            messageLabel.setTextFormat(messageFormat);
            addChild(messageLabel);
             
            logoImg = new ImageAsset();
            logoImg.width = 144;
            logoImg.height = 14;
            
            logoButton = new SimpleButton(logoImg, logoImg, logoImg, logoImg);
            logoButton.addEventListener(MouseEvent.CLICK, onLogoClicked);
            logoButton.width = 144;
            logoButton.height = 14;
            addChild(logoButton);
            
            onResize(null);
            
            stage.addEventListener(Event.RESIZE, onResize);
        }
        
        public function onLogoClicked(e:MouseEvent):void {
            var url:URLRequest = new URLRequest("http://dropbeat.net");
            navigateToURL(url, "_blank");
        }
        
        public function detach():void {
            if (stage != null) {
                stage.removeChild(this);
                stage.removeEventListener(Event.RESIZE, onResize);
            }
        }
        
        public function onResize(e:Event):void {
            
            var stageWidth:Number = stage.stageWidth;
            var stageHeight:Number = stage.stageHeight;
            
            graphics.clear();
            graphics.beginFill(BG_COLOR);
            graphics.lineStyle(1, BORDER_COLOR);
            graphics.drawRect(0, 0, stageWidth - 1, stageHeight - 1);
            graphics.endFill();

            logoButton.x = (stageWidth - logoImg.width) / 2;
            logoButton.y = stageHeight - logoImg.height - 20;
            
            messageLabel.width = stageWidth - 40;
            messageLabel.x = 20;
            
            var bodyHeight = stageHeight - (logoButton.height + 20);
            var centerHeight:Number = titleLabel.height + messageLabel.height + 10;
            messageLabel.y = (bodyHeight - centerHeight) / 2 + titleLabel.height + 10;
            
            titleLabel.x = (stageWidth - titleLabel.width) / 2;
            titleLabel.y = (bodyHeight - centerHeight) / 2;
        }
    }
}