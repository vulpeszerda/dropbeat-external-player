package {
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.net.URLRequest;
	import flash.system.Security;
	import flash.utils.Timer;

    public class YoutubePlayer extends BasePlayer{
        private var loader:Loader;
        private var player:Object;
        private var trackId:String;
        private var playerReady:Boolean = true;
        private var progressTimer:Timer;
        private var isFirstTick:Boolean = true;
        
        public function YoutubePlayer() {
            Security.allowDomain("www.youtube.com");
            loader = new Loader();
            loader.load(new URLRequest("http://youtube.com/apiplayer?version=3"));
            loader.contentLoaderInfo.addEventListener(Event.INIT, onLoaderInit);
        }
        

        override public function load(id:String, duration:int=-1):Boolean {
            if (player != null && trackId != id && playerReady) {
                isFirstTick = true;
                player.loadVideoById(id, 0);
                player.mute();
            }
            trackId = id;
            setState(PlayState.LOADING);
            return true;
        }

        override public function play():Boolean {
            if (player == null || !playerReady) {
                return false;
            }
            player.playVideo();
            return true;
        }
        
        override public function pause():Boolean {
            if (player == null || !playerReady) {
                return false;
            }
            player.pauseVideo();
            return true;
        }

        override public function stop():Boolean {
            if (player != null) {
                player.stopVideo();
            }
            trackId = null;
            setState(PlayState.IDEL);
            return true;
        }
        
        override public function seek(time:int):Boolean {
            if (this.state != PlayState.PLAYING || player == null || !playerReady) {
                return false
            }
            player.seekTo(time / 1000, true);
            return true;
        }
        
        override public function getPlayback():int {
            return player.getCurrentTime() * 1000;
        }
        
        override public function getDuration():int {
            if (player == null || !playerReady) {
                return 0;
            }
            return player.getDuration() * 1000;
        }

        override public function getType():String {
            return "youtube";
        }

        public function onLoaderInit(e:Event):void {
            player = loader.content;
            player.addEventListener("onReady", onPlayerReady);
            player.addEventListener("onError", onPlayerError);
            player.addEventListener("onStateChange", onPlayerStateChange);
        }

        public function onPlayerReady(e:Event):void {
            player.setSize(213, 120);
            playerReady = true;

            if (trackId != null) {
                isFirstTick = true;
                player.loadVideoById(trackId, 0);
                player.mute();
                setState(PlayState.LOADING);
            }
        }
        
        public function onPlayerEnded():void {
            isFirstTick = true;
            play();
            listener.onTick();
        }
        
        public function onPlayerPlaying():void {
            if (isFirstTick) {
                setState(PlayState.READY_TO_PLAY);
                isFirstTick = false;
                pause();
                return;
            }
            setState(PlayState.PLAYING);
            if (progressTimer != null) {
                progressTimer.stop();
                progressTimer.removeEventListener(TimerEvent.TIMER, onTick);
            }

            progressTimer = new Timer(250);
            progressTimer.addEventListener(TimerEvent.TIMER, onTick);
            progressTimer.start();
        }
        
        public function onPlayerPaused():void {
            if (this.state == PlayState.READY_TO_PLAY) {
                player.unMute();
                listener.onPlayerReady();
                return;
            }
            setState(PlayState.PAUSED);
            if (progressTimer != null) {
                progressTimer.stop();
                progressTimer.removeEventListener(TimerEvent.TIMER, onTick);
            }
        }
        
        public function onPlayerBuffering():void {
            
        }
        
        public function onPlayerCued():void {
        }
        
        public function onPlayerError(e:Event):void {
            trace("player error:", Object(e).data);
            stop();
            listener.onFailure(e);
        }
        
        public function onPlayerStateChange(e:Event):void {
            var state:int = Object(e).data;
            switch(state) {
                case -1: // YT.PlayerState.UNSTARTED
                    trace("unstarted");
                    break;
                case 0: // YT.PlayerState.ENDED
                    trace("ended");
                    onPlayerEnded();
                    break;
                case 1: // YT.PlayerState.PLAYING
                    trace("playing");
                    onPlayerPlaying();
                    break;
                case 2: // YT.PlayerState.PAUSED
                    trace("paused");
                    onPlayerPaused();
                    break;
                case 3: // YT.PlayerState.BUFFERING
                    trace("buffering");
                    onPlayerBuffering();
                    break;
                case 5: // YT.PlayerState.CUED
                    trace("cued");
                    onPlayerCued();
                    break;
            }
        }

        public function onTick(e:TimerEvent):void {
            listener.onTick(); 
        }
        
        public function getVideoView():Loader {
            return loader;
        }
    }
}